# GitLab CI template for pre-commit

This project implements a GitLab CI/CD template to build, test and analyse your [pre-commit](https://gitlab.com/to-be-continuous) projects.

## Usage

This template can be used both as a [CI/CD component](https://docs.gitlab.com/ee/ci/components/#use-a-component-in-a-cicd-configuration) 
or using the legacy [`include:project`](https://docs.gitlab.com/ee/ci/yaml/index.html#includeproject) syntax.

### Use as a CI/CD component

Add the following to your `gitlab-ci.yml`:

```yaml
include:
  # 1: include the component
  - component: gitlab.com/to-be-continuous/pre-commit/gitlab-ci-pre-commit@1.0.0
    # 2: set/override component inputs
    inputs:
      # ⚠ this is only an example
      build-args: "build --with-my-args"
```

### Use as a CI/CD template (legacy)

Add the following to your `gitlab-ci.yml`:

```yaml
include:
  # 1: include the template
  - project: 'to-be-continuous/pre-commit'
    ref: '1.0.0'
    file: '/templates/gitlab-ci-pre-commit.yml'

variables:
  # 2: set/override template variables
  # ⚠ this is only an example
  PRE_COMMIT_ARGS: "build --with-my-args"
```

## Global configuration

The pre-commit template uses some global configuration used throughout all jobs.

| Input / Variable      | Description                            | Default value     |
| --------------------- | -------------------------------------- | ----------------- |
| `pre-commit-image` / `PRE_COMMIT_IMAGE` | The Docker image used to run `pre-commit` | `registry.hub.docker.com/pre-commit:1.0.0` |

## Jobs

### `pre-commit` job

This job performs perfom pre-commit hooks check

It uses the following variable:

| Input / Variable      | Description                              | Default value     |
| --------------------- | ---------------------------------------- | ----------------- |
| `pre-commit-disabled` / `PRE_COMMIT_DISABLED` | Disable pre-commit job | `false` |
| `pre-commit-args` / `PRE_COMMIT_ARGS` | Additionnal arguments for the pre-commit command | `` |


### Secrets management

Here are some advices about your **secrets** (variables marked with a :lock:):

1. Manage them as [project or group CI/CD variables](https://docs.gitlab.com/ee/ci/variables/#create-a-custom-variable-in-the-ui):
    * [**masked**](https://docs.gitlab.com/ee/ci/variables/#mask-a-custom-variable) to prevent them from being inadvertently
      displayed in your job logs,
    * [**protected**](https://docs.gitlab.com/ee/ci/variables/#protect-a-custom-variable) if you want to secure some secrets
      you don't want everyone in the project to have access to (for instance production secrets).
2. In case a secret contains [characters that prevent it from being masked](https://docs.gitlab.com/ee/ci/variables/#masked-variable-requirements), 
  simply define its value as the [Base64](https://en.wikipedia.org/wiki/Base64) encoded value prefixed with `@b64@`:
  it will then be possible to mask it and the template will automatically decode it prior to using it.
3. Don't forget to escape special characters (ex: `$` -> `$$`).
